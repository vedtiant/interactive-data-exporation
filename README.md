# Datascope 

DataScope is an interactive dashboard system for doing exploratory analysis on large biomedical datasets.  Each dashboard is described through a set of 4 JSON files that describe the data sources, the data, the filters and the resulting display. Datascope comes with a few basic visualizations. 

### Bleeding edge deployment guide ###

##### Prerequisites
* Node.js
* Grunt ``` npm install grunt-cli -g ``` (might require root)

##### Installation
* Clone the repository
* Switch to dev branch ```git checkout dev``` 
* ```npm install``` (might require root)
* On the project root run ```grunt browserify``` 

##### Running
* Create ```public/config``` directory.
* Run ```node app.js```
* Open ```http://localhost:3001```

### Quick start guide(Stable version) ###

##### Installation

* Install Node.js
* Clone the repository
* Run ```npm install```

##### Running

* Modify the files present in ```public/config``` to fit your needs:
    * dataSource.json (Refer to [dataSource.json documentation](https://bitbucket.org/BMI/interactive-data-exporation/wiki/dataSource.json))
    * dataDescription.json (Refer to [dataDescription.json documentation](https://bitbucket.org/BMI/interactive-data-exporation/wiki/dataDescription.json))
    * interactiveFilters.json (Refer to [interactiveFilters.json documentation](https://bitbucket.org/BMI/interactive-data-exporation/wiki/interactiveFilters.json))
    * visualization.json (Refer to [visualization.json documentation](https://bitbucket.org/BMI/interactive-data-exporation/wiki/visualization.json))


* Run ```node app.js```
* Goto ```http://localhost:3000``` from your favorite browser.

Read the User Guide present in ```docs/wiki/``` for more details
